<?php
class accountController extends framework{
public function __construct(){
  $this->helper("link");
  $this->accountModel = $this->model('accountModel');
}
public function index(){
  $this->view("signup");
}
public function createAccount(){
  $userData = [
    'fullName'       => $this->input('fullName'),
    'email'          => $this->input('email'),
    'password'       => $this->input('password'),
    'fullNameError'  => '',
    'emailError'     => '',
    'passwordError'  => ''
  ];
  if(empty($userData['fullName'])){
    $userData['fullNameError'] = 'Full name is required';
  }
  if(empty($userData['email'])){
    $userData['emailError'] = 'Email is required';
  }else{
    if(!$this->accountModel->checkEmail($userData['email'])){
      $userData['emailError']= "Sorry this email is already exist";
    }
  }
  if(empty($userData['password'])){
    $userData['passwordError'] = "Password is required";
  }else if(strlen($userData['password']) < 5){
    $userData['passwordError'] = "Passwords must be 5 characters long";
  }
  if(empty($userData['fullNameError']) && empty($userData['emailError']) && empty($userData['passwordError'])){
    $password = password_hash($userData['password'], PASSWORD_DEFAULT);
    $data= [$userData['fullName'], $userData['email'], $password ];
    if($this->accountModel->createAccount($data)){
      echo "Your account has been created successfully";
    }
  } else {
    $this->view('signup',$userData);
  }
}
public function loginForm(){
  $this->view("login");
}
}
?>
