-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2019 at 09:45 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mvcdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullName` varchar(20) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullName`, `email`, `password`) VALUES
(1, 'Fatin Fuad Karim', 'fatinfuadkarim@gamil.com', '123456'),
(2, 'Fatin Fuad Karim', 'fatinfuadkarim@gamil.com', '123456'),
(3, 'rahma', 'rahma@gmail.com', ''),
(4, 'rahma', 'rahma@gmail.com', ''),
(5, 'rahma', 'rahma@gmail.com', ''),
(6, 'rahma', 'rahma@gmail.com', ''),
(7, 'rahma', 'rahma@gmail.com', ''),
(8, 'rahma', 'rahma@gmail.com', ''),
(9, 'rahma', 'rahma@gmail.com', ''),
(10, '1', '1', '1'),
(11, '', '', ''),
(13, '1', '1', '1'),
(14, '1', '1', '1'),
(15, 'Shakib Al Hasan', 'sah75@gmail.com', '123456'),
(16, '', '', ''),
(17, '', '', ''),
(18, '', '', ''),
(19, '', '', ''),
(20, '', '', ''),
(21, '', '', ''),
(22, '', '', ''),
(23, '', '', ''),
(24, '', '', ''),
(25, 'bithi', 'bithi@gmail.com', '1234'),
(26, 'Tamim Iqbal', 'tamimiqbal28@gmail.com', '$2y$10$55q9TwWJH5f21c1/T.NQJ.yZi.wbM89pakZPjGO1xL5Nm0O6lJnm6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
